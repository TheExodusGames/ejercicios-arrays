﻿using System;

namespace _1
{
    class Program
    {
        static void Main(string[] args)
        { 
            double[] numeros = new double[4];
            double suma = 0;
            double Maritmetica;
            Console.WriteLine("Bienvenido al programa que pida 4 numeros, diga cuales son y saque la aritmetica");
            for(int cnt=0; cnt<4; cnt++){
                Console.Write("Introduzca el numero {0}: ", cnt+1); 
                numeros[cnt] = Convert.ToDouble(Console.ReadLine()); 
                suma += numeros[cnt]; 
                }
            Maritmetica = suma / 4;
            Console.WriteLine("----------------------------------");
            Console.Write("Los numeros ingresados fueron: ");
            for(int cnt=0; cnt<4; cnt++){
                Console.Write(" ({0}) ", numeros[cnt]); 
            }
            Console.WriteLine("\nY la media aritmetica es: {0}", Maritmetica);
        }
    }
}
/* Made by Alexander Coronado */