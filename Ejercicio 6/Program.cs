﻿using System;

namespace _6
{
    class Program
    {      struct tipoFicha {
            public string nombreFich;
            public long AnchoP;
            public long AlturaP;
            public long tamanyo;
        };
        static void Main(string[] args)
        {
            tipoFicha[] fichas = new tipoFicha[700];
            int numeroFichas=0;
            int c;               
            int opcion;          
            string textoBuscar;  
            long tamanyoBuscar; 
            Console.WriteLine("Bienvenidos al algoritmo que almacene imagenes con sus datos: Nombre, Altura, Anchura, Tamano");
            do {
      // Menu principal, repetitivo
            Console.WriteLine();
            Console.WriteLine("Escoja una opción:");
            Console.WriteLine("1.- Añadir datos de una nueva imagen");
            Console.WriteLine("2.- Mostrar los nombres de todas las imagenes");
            Console.WriteLine("3.- Mostrar imagenes por encima de un cierto tamaño");
            Console.WriteLine("4.- Mostrar imagenes por nombres");
            Console.WriteLine("5.- Salir");

            opcion = Convert.ToInt32( Console.ReadLine() );
            
            switch(opcion) {
 
            case 1:
                if (numeroFichas < 1000) { 
                    Console.WriteLine("Introduce el nombre de la imagen: ");
                    fichas[numeroFichas].nombreFich = Console.ReadLine();
                    Console.WriteLine("Introduce la anchura de Pixeles: ");
                    fichas[numeroFichas].AnchoP  = int.Parse(Console.ReadLine());
                    Console.WriteLine("Introduce la altura de Pixeles: ");
                    fichas[numeroFichas].AlturaP  = int.Parse(Console.ReadLine());
                    Console.WriteLine("Introduce el tamaño en KB: ");
                    fichas[numeroFichas].tamanyo  = int.Parse(Console.ReadLine());
                    numeroFichas++;
                } else
                    Console.WriteLine("Máximo de fichas alcanzado (1000)!");
            break;
 
            case 2:
                for (c=0; c<numeroFichas; c++) 
                    Console.WriteLine("Nombre: {0}; Anchura: {1} KB; Altura: {2}; Tamaño: {3} KB", 
                    fichas[c].nombreFich, fichas[c].AnchoP, fichas[c].AlturaP, fichas[c].tamanyo);
            break;
 
            case 3:
                Console.WriteLine("¿A partir de que tamaño quieres ver?");
                tamanyoBuscar = Convert.ToInt64( Console.ReadLine() );

                for (c=0; c<numeroFichas; c++)
                    if (fichas[c].tamanyo >= tamanyoBuscar)
                        Console.WriteLine("Nombre: {0}; Anchura: {1} KB; Altura: {2}; Tamaño: {3} KB",
                        fichas[c].nombreFich, fichas[c].AnchoP, fichas[c].AlturaP, fichas[c].tamanyo);
            break;
 
            case 4:
                Console.WriteLine("¿De qué imagen quieres ver todos los datos?");
                textoBuscar = Console.ReadLine();

                for (c=0; c<numeroFichas; c++) 
                    if ( fichas[c].nombreFich == textoBuscar )
                        Console.WriteLine("Nombre: {0}; Anchura: {1} KB; Altura: {2}; Tamaño: {3} KB",
                        fichas[c].nombreFich, fichas[c].AnchoP, fichas[c].AlturaP, fichas[c].tamanyo);
            break;
 
            case 5:
                Console.WriteLine("Fin del programa, gracias por utilizarnos."); 
            break;
 
            default:
                Console.WriteLine("-------------------------------------------------------"); 
                Console.WriteLine("No se reconoce esta opcion, lea bien y seleccione una."); 
                Console.WriteLine("-------------------------------------------------------");
            break;
            }
        } while (opcion != 5);
        }
    }
}
/* Made by Alexander Coronado */