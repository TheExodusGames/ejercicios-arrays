﻿using System;

namespace ejercicio_9_array
{
    class Program
    {
        static void Main(string[] args)
        {
            ejercicio_9 ejer9 = new ejercicio_9();
            ejer9.ingresar_numero();
        }
    }

    class ejercicio_9
    {
        public void tabla_multiplicar(int v)
        {
            int cont = 0, result;

            for (cont = 1; cont <= 12; cont++)
            {
                result = cont * v;
                Console.WriteLine(cont + " X " + v + " = " + result);
            }
        }

        public void ingresar_numero()
        {
            int v_entero;
            Console.WriteLine("Bienvenido al programa que acepte valores enteros y haga una tabla de multiplicar.");
            do
            {
                Console.WriteLine("\nPara finalizar el algoritmo digite -1");
                Console.WriteLine("Ingrese un valor: ");
                v_entero = int.Parse(Console.ReadLine());
                if (v_entero != -1)
                {
                    tabla_multiplicar(v_entero);
                }
                else
                {
                    Console.WriteLine("\n---------------------------------------------");
                    Console.WriteLine("Programa Finalizado, gracias por utilizarnos.");
                    Console.WriteLine("---------------------------------------------\n");
                }
            } while (v_entero != -1);
        }
    }
}
/* Made by Alexander Coronado */
