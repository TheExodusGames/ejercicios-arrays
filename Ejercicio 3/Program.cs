﻿using System;

namespace _3
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] meses = {
                0,
                31,
                28,
                31,
                30,
                31,
                30,
                31,
                31,
                30,
                31,
                30,
                31
            };
            Console.WriteLine("Bienvenido al programa para ver cuantos dias tiene un mes.");
            Console.WriteLine("Digite el numero del mes que quiere consultar.");
            int mes = int.Parse(Console.ReadLine());
            int r = meses[mes];
            Console.WriteLine("El mes numero {0} ", mes + " Tiene: " + r + " dias");
        }
    }
}
/* Made by Alexander Coronado */