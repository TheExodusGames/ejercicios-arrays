﻿using System;

namespace Ejercicio10
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Bienvenido al programa que cargue el nombre de una persona y su edad y determine si es mayor de edad o no.");
            Datos_persona dat_per = new Datos_persona();
            dat_per.ingresar_datos();
            dat_per.mostrar_datos();
            dat_per.mayor_edad();
        }
    }
    class Datos_persona
    {
         string nombre;
         int edad;

         public void ingresar_datos()
         {
             Console.Write("Ingrese el nombre: ");
             nombre = Console.ReadLine();
             Console.Write("Ingrese la edad: ");
             edad = int.Parse(Console.ReadLine());
         }

         public void mostrar_datos()
         {
             Console.WriteLine("\n" + "Nombre: " + nombre);
             Console.WriteLine("Edad: " + edad);
         }

         public void mayor_edad()
         {
             if (edad >= 18)
             {
                Console.WriteLine("Es mayor de edad");
             }
             else
             {
                 Console.WriteLine("-------------------");
                 Console.WriteLine("No es mayor de edad");
                 Console.WriteLine("-------------------");
             }
             Console.ReadKey();
         }

    }
}
/* Made by Alexander Coronado */