﻿using System;

namespace _7
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Bienvenido al Programa para separar nombres por letras.");
             string nombre;
            
            Console.WriteLine("ingrese su nombre:");
            nombre = Console.ReadLine();
            char[] esp = nombre.ToCharArray();
            foreach (char n in esp)
            {
                Console.Write(n + " ");
            }
            Console.ReadKey();
        }
    }
}
/* Made by Alexander Coronado */