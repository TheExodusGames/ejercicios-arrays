﻿using System;

namespace _2
{
    class Program
    {
        static void Main(string[] args)
        {
         int [] numeros = new int[5];
            Console.WriteLine("Bienvenido al programa que reciba 5 numeros, y despues los muestre invertidos.");
            for(int cnt=0; cnt<5; cnt++){
                Console.Write("Introduzca el numero {0}: ", cnt+1); 
                numeros[cnt] = int.Parse(Console.ReadLine());
                }

             for(int cnt2=5; cnt2>0; cnt2--){
                 int numero = numeros[cnt2-1];
                 Console.WriteLine("El numero {0} ", cnt2 + " es: " + numero);
             }
        }
    }
}
/* Made by Alexander Coronado */